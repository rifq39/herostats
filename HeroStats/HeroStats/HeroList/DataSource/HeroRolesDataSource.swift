//
//  HeroRolesDataSource.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 18/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import UIKit

protocol HeroRolesDataSourceProtocol: class {
    func onTapRole(rolesSelected: String)
}

class HeroRolesDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var listRoles: [String] = []
    var delegate: HeroRolesDataSourceProtocol?
    
    init(delegate: HeroRolesDataSourceProtocol?) {
        self.delegate = delegate
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listRoles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)
        cell.selectionStyle = .none
        let listRolesString = Array(listRoles)
        cell.textLabel?.text = listRolesString[indexPath.section]
        cell.indentationLevel = 0
        cell.textLabel?.textColor = .white
        cell.textLabel?.textAlignment = .center
        cell.contentView.layer.cornerRadius = 20
        cell.contentView.backgroundColor = .black
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rolesSelected = listRoles[indexPath.section]
        delegate?.onTapRole(rolesSelected: rolesSelected)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}

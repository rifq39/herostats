//
//  HeroGridAdapter.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 18/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import UIKit
import ObjectMapper
import Imaginary

protocol HeroListDataSourceProtocol: class {
    func onTapHero(hero: HeroModel)
}

class HeroListDataSource: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {
    var heroList: [HeroModel] = []
    var heroItemSize: CGSize = CGSize(width: 200, height: 100)
    var delegate: HeroListDataSourceProtocol?
    
    init(delegate: HeroListDataSourceProtocol?) {
        self.delegate = delegate
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return heroList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HeroItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeroItemCell", for: indexPath) as! HeroItemCell
        if let url = URL(string: Constants.URL + heroList[indexPath.row].img) {
            cell.imageView.setImage(url: url)
        }
        cell.labelText.text = "\(heroList[indexPath.row].localized_name)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let heroModel = heroList[indexPath.row]
        delegate?.onTapHero(hero: heroModel)
    }
    
}

extension HeroListDataSource: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return heroItemSize
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10) //.zero
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 30
    }
}

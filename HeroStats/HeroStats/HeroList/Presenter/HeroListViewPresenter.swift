//
//  HeroListViewModel.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 17/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import RealmSwift

protocol HeroListViewPresenterProtocol: class {
    func loadData(heroList: [HeroModel])
    func showError(message: String)
}

class HeroListViewPresenter {
    private var service: APIService = APIService()
    private weak var delegate: HeroListViewPresenterProtocol?
    private let realm = try! Realm()
    
    init(delegate: HeroListViewPresenterProtocol?) {
        self.delegate = delegate
    }

    func fetchHeroList() {
        if let reachability = NetworkReachabilityManager(),
            reachability.isReachable {
            if let heroList = getCache(), !heroList.isEmpty {
                delegate?.loadData(heroList: Array(heroList))
            } else {
                service.fetchHeroList { [weak self] (response, error) in
                    guard let ws = self else { return }
                    if let response = response {
                        try! ws.realm.write {
                            ws.realm.add(response)
                        }
                        ws.delegate?.loadData(heroList: Array(response.heroList))
                    } else if let _ = error {
                        ws.delegate?.showError(message: "Error Request")
                    }
                }
            }
        } else {
            if let heroList = getCache(), !heroList.isEmpty {
                delegate?.loadData(heroList: Array(heroList))
            } else {
                delegate?.showError(message: "No Internet connection")
            }
        }
        
    }
    
    func getCache() -> Results<HeroModel>? {
        return realm.objects(HeroModel.self)
    }
    
    func sort(by role: String) {
        if let heroList = getCache() {
            if role == "All" {
                delegate?.loadData(heroList: Array(heroList))
            } else {
                let filterByRole = heroList.filter({ $0.roles.contains(role) })
                delegate?.loadData(heroList: Array(filterByRole))
            }
        }
    }

}

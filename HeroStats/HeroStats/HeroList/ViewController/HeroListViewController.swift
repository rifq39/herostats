//
//  HeroListViewController.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 17/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import UIKit
import ObjectMapper

class HeroListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    private var heroListViewPresenter: HeroListViewPresenter?
    private var heroRolesDataSource: HeroRolesDataSource?
    private var heroListDataSource: HeroListDataSource?
    private var listRoles: Set<String> = []
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeRight
    }
    
    var listHero: [HeroModel] = []
    
    func set(presenter: HeroListViewPresenter) {
        heroListViewPresenter = presenter
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = Constants.rolesDefault
        setupDataSource()
        setupTableView()
        setupCollectionView()
        heroListViewPresenter?.fetchHeroList()
    }
    
    private func setupDataSource() {
        heroRolesDataSource = HeroRolesDataSource(delegate: self)
        heroListDataSource = HeroListDataSource(delegate: self)
    }
    
    private func setupTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellId")
        tableView.delegate = heroRolesDataSource
        tableView.dataSource = heroRolesDataSource
        tableView.separatorColor = .clear
        tableView.showsVerticalScrollIndicator = false
    }
    
    private func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        collectionView.delegate = heroListDataSource
        collectionView.dataSource = heroListDataSource
        collectionView.collectionViewLayout = flowLayout
        collectionView.register(UINib(nibName: "HeroItemCell", bundle: nil), forCellWithReuseIdentifier: "HeroItemCell")
        
    }
    
    func rolesData(heroList: [HeroModel]) {
        for hero in heroList {
        let roles = hero.roles
            let unionSet = listRoles.union(roles)
            listRoles = unionSet
        }
        listRoles.insert(Constants.rolesDefault)
        heroRolesDataSource?.listRoles = Array(listRoles)
    }
    
    func loadData(heroList: [HeroModel]) {
        heroListDataSource?.heroList = heroList
        rolesData(heroList: heroList)
        tableView.reloadData()
        collectionView.reloadData()
    }
    
    func onTapRole(rolesSelected: String) {
        navigationItem.title = rolesSelected
        heroListViewPresenter?.sort(by: rolesSelected)
    }
    
    func onTapHero(hero: HeroModel) {
        
         let HeroDetailVC = HeroDetailViewController(nibName: "HeroDetailViewController", bundle: nil)
        let presenter = HeroDetailViewPresenter()
        presenter.set(heroModel: hero)
        HeroDetailVC.set(presenter: presenter)
        navigationController?.pushViewController(HeroDetailVC, animated: true)
    }
    
    func showError(message: String) {
        let alert = UIAlertController(title: message,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK",
                                      style: .destructive,
                                      handler: { [weak self] alertAction in
                                        self?.dismiss(animated: false, completion: nil)
        }))
        self.present(alert, animated: false, completion: nil)
    }
    
}

extension HeroListViewController: HeroListViewPresenterProtocol { }
extension HeroListViewController: HeroRolesDataSourceProtocol { }
extension HeroListViewController: HeroListDataSourceProtocol { }

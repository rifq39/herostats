//
//  HeroDetailViewController.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 19/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import UIKit
import Imaginary

class HeroDetailViewController: UIViewController {
    @IBOutlet weak var similiarHerosCV: UICollectionView!
    
    private var presenter: HeroDetailViewPresenter?
    private var heroListDataSource: HeroListDataSource?
    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var heroIcon: UIImageView!
    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var heroRoles: UILabel!
    @IBOutlet weak var attackLabel: UILabel!
    @IBOutlet weak var armorLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var healthLabel: UILabel!
    @IBOutlet weak var manaLabel: UILabel!
    @IBOutlet weak var primaryAttLabel: UILabel!
   
    @IBOutlet weak var similiarLabel: UILabel!
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeRight
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .landscapeRight
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        setupCollectionView()
        setupUI()
    }
    
    func set(presenter: HeroDetailViewPresenter) {
        self.presenter = presenter
    }
    
    private func setupDataSource() {
        heroListDataSource = HeroListDataSource(delegate: self)
        heroListDataSource?.heroItemSize = CGSize(width: 120, height: 100)
    }
    
    private func setupCollectionView() {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        similiarHerosCV.delegate = heroListDataSource
        similiarHerosCV.dataSource = heroListDataSource
        similiarHerosCV.collectionViewLayout = flowLayout
        similiarHerosCV.register(UINib(nibName: "HeroItemCell", bundle: nil), forCellWithReuseIdentifier: "HeroItemCell")
        
    }

    private func setupUI() {
        guard let hero = presenter?.getHeroModel() else { return }
        if let url = URL(string: Constants.URL + hero.img) {
            heroImage.setImage(url: url)
        }
        if let url = URL(string: Constants.URL + hero.icon) {
            heroIcon.setImage(url: url)
        }
        heroName.text = hero.localized_name
        heroRoles.text = "Role: " + "\(Array(hero.roles).joined(separator: ", "))"
        
        attackLabel.text = "\(hero.base_attack_min) - \(hero.base_attack_max)"
        armorLabel.text = "\(hero.base_armor)"
        speedLabel.text = "\(hero.move_speed)"
        healthLabel.text = "\(hero.base_health)"
        manaLabel.text = "\(hero.base_mana)"
        primaryAttLabel.text = hero.primary_attr
        
        if let hero = presenter?.getSimiliarHero(hero: hero) {
            heroListDataSource?.heroList = hero
            similiarLabel.text = "Simliar Heros"
        } else {
            heroListDataSource?.heroList = []
            similiarLabel.text = ""
            
        }
        similiarHerosCV.reloadData()
    }
}

extension HeroDetailViewController: HeroListDataSourceProtocol {
    func onTapHero(hero: HeroModel) { }
}

//
//  HeroDetailPresenterswift.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 19/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import RealmSwift

class HeroDetailViewPresenter {
    private var heroModel: HeroModel?
    private let realm = try! Realm()
    
    func set(heroModel: HeroModel) {
        self.heroModel = heroModel
    }
    
    func getHeroModel() -> HeroModel? {
        return heroModel
    }
    
    func getCache() -> Results<HeroModel>? {
        return realm.objects(HeroModel.self)
    }
    
    func getSimiliarHero(hero: HeroModel) -> [HeroModel]? {
        if let heroList = getCache() {
            if hero.primary_attr == "agi" {
                let filterHero = heroList.filter { $0.primary_attr == hero.primary_attr }
                let sortedHero = filterHero.sorted { $0.move_speed > $1.move_speed && $0.localized_name != hero.localized_name }
                let jsutThree = sortedHero.prefix(upTo: 3)
                return Array(jsutThree)
            } else if hero.primary_attr == "str" {
                let filterHero = heroList.filter { $0.primary_attr == hero.primary_attr }
                let sortedHero = filterHero.sorted { $0.base_attack_max > $1.base_attack_max && $0.localized_name != hero.localized_name }
                let jsutThree = sortedHero.prefix(upTo: 3)
                return Array(jsutThree)
            } else if hero.primary_attr == "int" {
                let filterHero = heroList.filter { $0.primary_attr == hero.primary_attr }
                let sortedHero = filterHero.sorted { $0.base_mana > $1.base_mana && $0.localized_name != hero.localized_name }
                let jsutThree = sortedHero.prefix(upTo: 3)
                return Array(jsutThree)
            }
        }
        
        return nil
    }

    
}

//
//  HeroListModel.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 17/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class HeroListModel: Object, Mappable {
    var heroList: List = List<HeroModel>()
    //Impl. of Mappable protocol
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        var tempHeroList: [HeroModel] = []
        tempHeroList <- map["data"]
        for i in tempHeroList {
            heroList.append(i)
        }
    }

}

//
//  HeroModel.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 17/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import ObjectMapper
import RealmSwift

public class HeroModel: Object, Mappable {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var localized_name: String = ""
    @objc dynamic var primary_attr: String = ""
    @objc dynamic var attack_type: String = ""
    var roles: List = List<String>()
    @objc dynamic var img: String = ""
    @objc dynamic var icon: String = ""
    @objc dynamic var base_health: Int = 0
    @objc dynamic var base_health_regen: Float  = 0
    @objc dynamic var base_mana: Int = 0
    @objc dynamic var base_mana_regen: Int = 0
    @objc dynamic var base_armor: Int = 0
    @objc dynamic var base_mr: Int = 0
    @objc dynamic var base_attack_min: Float  = 0
    @objc dynamic var base_attack_max: Int = 0
    @objc dynamic var base_str: Int = 0
    @objc dynamic var base_agi: Int = 0
    @objc dynamic var base_int: Int = 0
    @objc dynamic var str_gain: Float  = 0
    @objc dynamic var int_gain: Float = 0
    @objc dynamic var attack_range: Int = 0
    @objc dynamic var projectile_speed: Int = 0
    @objc dynamic var attack_rate: Float = 0
    @objc dynamic var move_speed: Int  = 0
    @objc dynamic var turn_rate: Float = 0
    @objc dynamic var cm_enabled: Bool = false
    @objc dynamic var legs: Int = 0
    @objc dynamic var pro_ban: Int = 0
    @objc dynamic var hero_id: Int  = 0
    @objc dynamic var pro_win: Int = 0
    @objc dynamic var pro_pick: Int = 0
    @objc dynamic var one_pick: Int = 0
    @objc dynamic var one_win: Int = 0
    @objc dynamic var two_pick: Int  = 0
    @objc dynamic var two_win: Int = 0
    @objc dynamic var three_pick: Int = 0
    @objc dynamic var three_win: Int = 0
    @objc dynamic var four_pick: Int = 0
    @objc dynamic var four_win: Int  = 0
    @objc dynamic var five_pick: Int = 0
    @objc dynamic var five_win: Int = 0
    @objc dynamic var six_pick: Int = 0
    @objc dynamic var six_win: Int = 0
    @objc dynamic var seven_pick: Int = 0
    @objc dynamic var seven_win: Int = 0
    @objc dynamic var eight_pick: Int  = 0
    @objc dynamic var eight_win: Int = 0
    @objc dynamic var null_pick: Int = 0
    @objc dynamic var null_win: Int = 0
    
    
    //Impl. of Mappable protocol
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        localized_name <- map["localized_name"]
        primary_attr <- map["primary_attr"]
        attack_type <- map["attack_type"]
        var tempRoles: [String]!
        tempRoles <- map["roles"]
        for i in tempRoles {
            roles.append(i)
        }
        roles <- map["roles"]
        img <- map["img"]
        icon <- map["icon"]
        base_health <- map["base_health"]
        base_health_regen <- map["base_health_regen"]
        base_mana <- map["base_mana"]
        base_mana_regen <- map["base_mana_regen"]
        
        base_armor <- map["base_armor"]
        base_mr <- map["base_mr"]
        base_attack_min <- map["base_attack_min"]
        base_attack_max <- map["base_attack_max"]
        base_str <- map["base_str"]
        base_agi <- map["base_agi"]
        base_int <- map["base_int"]
        str_gain <- map["str_gain"]
        int_gain <- map["int_gain"]
        attack_range <- map["attack_range"]
        projectile_speed <- map["projectile_speed"]
        attack_rate <- map["attack_rate"]
        move_speed <- map["move_speed"]
        
        turn_rate <- map["turn_rate"]
        cm_enabled <- map["cm_enabled"]
        legs <- map["legs"]
        hero_id <- map["hero_id"]
        pro_win <- map["pro_win"]
        pro_pick <- map["pro_pick"]
        one_pick <- map["1_pick"]
        one_win <- map["1_win"]
        two_pick <- map["2_pick"]
        two_win <- map["2_win"]
        three_pick <- map["3_pick"]
        three_win <- map["3_win"]
        four_pick <- map["4_pick"]
        four_win <- map["4_win"]
        
        five_pick <- map["5_pick"]
        five_win <- map["5_win"]
        six_pick <- map["6_pick"]
        seven_pick <- map["7_pick"]
        seven_win <- map["7_win"]
        eight_pick <- map["8_pick"]
        eight_win <- map["8_win"]
        null_pick <- map["null_pick"]
        null_win <- map["null_win"]
    }
}


//
//  HeroAPI.swift
//  HeroStats
//
//  Created by Muchamad Rifki on 17/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//
import UIKit
import Alamofire
import ObjectMapper

struct APIService {
    func fetchHeroList(completionHandler: @escaping (_ responseObject: HeroListModel?, _ error: NSError?) -> ()) {
        let request = AF.request(Constants.URL + "/api/herostats")
        request.responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json: [String: Any] = [
                    "data": value
                ]
                let heroArray = Mapper<HeroListModel>().map(JSONObject: json)
                completionHandler(heroArray, nil)
            case .failure(let error):
                completionHandler(nil, error as NSError)
            }
        }
    }
}
    

//
//  HeroListViewControllerSpec.swift
//  HeroStatsTests
//
//  Created by Muchamad Rifki on 21/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import Quick
import Nimble
@testable import HeroStats

final class HeroListViewControllerSpec: QuickSpec {
    override func spec() {
        var heroListVC: HeroListViewController!
        var presenter: HeroListViewPresenterMock!
        var heroListDataSource: HeroListDataSource!
        var heroRolesDataSource: HeroRolesDataSource!

        describe("HeroListViewControllerSpec") {
            beforeEach {
                heroListVC = HeroListViewController(nibName: "HeroListViewController", bundle: nil)
                presenter = HeroListViewPresenterMock(delegate: heroListVC)
                
                heroListVC.set(presenter: presenter)
                heroListDataSource = HeroListDataSource(delegate: heroListVC)
                heroRolesDataSource = HeroRolesDataSource(delegate: heroListVC)
                heroListVC.loadView()
            }
            
            it(".viewDidload, must fetch hero list and check not nof data source") {
                expect(presenter.isFetchHeroList).to(beFalse())
                
                heroListVC.viewDidLoad()
                expect(heroListVC.tableView.delegate).notTo(beNil())
                expect(heroListVC.tableView.dataSource).notTo(beNil())
                expect(heroListVC.collectionView.delegate).notTo(beNil())
                expect(heroListVC.collectionView.dataSource).notTo(beNil())
                //fetch hero list expect
                expect(presenter.isFetchHeroList).to(beTrue())

            }
            
            it("navigation item must set title is All") {
                heroListVC.collectionView.delegate = heroListDataSource
                heroListVC.collectionView.dataSource = heroListDataSource
                heroListVC.tableView.delegate = heroRolesDataSource
                heroListVC.tableView.dataSource =  heroRolesDataSource

                let rolesSelected = "All"
                heroListVC.onTapRole(rolesSelected: rolesSelected)
                
                expect(heroListVC.navigationItem.title).to(equal(rolesSelected))
            }
        }

    }
}

//
//  HeroListDataSourceSpec.swift
//  HeroStatsTests
//
//  Created by Muchamad Rifki on 20/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import Quick
import Nimble
@testable import HeroStats

final class HeroListDataSourceSpec: QuickSpec {
    override func spec() {
        var heroListDataSource: HeroListDataSource!
        var heroListDataSourceProtocolMock: HeroListDataSourceProtocolMock!
        var collectionView: UICollectionView!
        var layout: UICollectionViewFlowLayout!
        var heroListData: [HeroModel]!
        
        describe("HeroListDataSource") {
            
            beforeEach {
                heroListData = [HeroModel(), HeroModel()]
                heroListDataSourceProtocolMock = HeroListDataSourceProtocolMock()
                heroListDataSource = HeroListDataSource(delegate: heroListDataSourceProtocolMock)
                heroListDataSource.heroList = heroListData
                layout = UICollectionViewFlowLayout()
                collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
                collectionView.register(HeroItemCell.self,
                                        forCellWithReuseIdentifier: "HeroItemCell")
                collectionView.dataSource = heroListDataSource
                collectionView.delegate = heroListDataSource
            }
            
            //MARK: Datasource
            
            it("should return the right number of rows") {
                expect(heroListDataSource.collectionView(collectionView, numberOfItemsInSection: 0)).to(equal(heroListData.count))
            }
            
            it("should return the right number of sections") {
                expect(heroListDataSource.numberOfSections(in: collectionView)).to(equal(1))
            }
            
            //MARK: Delegate
            
            it("should return Rodrigo if user select first Cell") {
                let indexPath = IndexPath(row: 0, section: 0)

                expect(heroListDataSourceProtocolMock.onTapHero).to(beFalse())

                heroListDataSource.collectionView(collectionView, didSelectItemAt: indexPath)
                expect(heroListDataSourceProtocolMock.onTapHero).to(beTrue())
                expect(heroListDataSourceProtocolMock.heroModel).notTo(beNil())
            }
        }
    }
}

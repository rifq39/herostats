//
//  HeroRolesDataSourceSpec.swift
//  HeroStatsTests
//
//  Created by Muchamad Rifki on 21/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import Quick
import Nimble
@testable import HeroStats

final class HeroRolesDataSourceSpec: QuickSpec {
    override func spec() {
        var heroRolesDataSource: HeroRolesDataSource!
        var heroRolesDataSourceProtocolMock: HeroRolesDataSourceProtocolMock!
        var tableView: UITableView!
        var listRolesData: [String]!
        
        describe("HeroRolesDataSource") {
            beforeEach {
                listRolesData = ["test1", "test2"]
                heroRolesDataSourceProtocolMock = HeroRolesDataSourceProtocolMock()
                heroRolesDataSource = HeroRolesDataSource(delegate: heroRolesDataSourceProtocolMock)
                heroRolesDataSource.listRoles = listRolesData
                tableView = UITableView()
                tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellId")
                tableView.delegate = heroRolesDataSource
                tableView.dataSource = heroRolesDataSource
            }
            
            //MARK: Datasource
            
            it("must return the right number of rows") {
                expect(heroRolesDataSource.tableView(tableView, numberOfRowsInSection: 0)).to(equal(1))
            }
            
            it("must return the right number of sections") {
                expect(heroRolesDataSource.numberOfSections(in: tableView)).to(equal(listRolesData.count))
            }
            
            //MARK: Delegate
            
            it("must return not empty if user select first Cell") {
                let indexPath = IndexPath(row: 0, section: 0)
                
                expect(heroRolesDataSourceProtocolMock.onTapRole).to(beFalse())
                
                heroRolesDataSource.tableView(tableView, didSelectRowAt: indexPath)
                expect(heroRolesDataSourceProtocolMock.onTapRole).to(beTrue())
                expect(heroRolesDataSourceProtocolMock.rolesSelected).notTo(beEmpty())
            }

        }
        
    }
}

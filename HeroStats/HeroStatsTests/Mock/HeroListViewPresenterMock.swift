//
//  HeroListViewPresenterMock.swift
//  HeroStatsTests
//
//  Created by Muchamad Rifki on 21/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

@testable import HeroStats

class HeroListViewPresenterMock: HeroListViewPresenter {
    var isFetchHeroList: Bool = false
    override func fetchHeroList() {
        isFetchHeroList = true
    }
}


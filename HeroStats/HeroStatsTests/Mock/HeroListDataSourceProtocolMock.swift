//
//  HeroListDataSourceMock.swift
//  HeroStatsTests
//
//  Created by Muchamad Rifki on 20/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

import Foundation
@testable import HeroStats

class HeroListDataSourceProtocolMock: HeroListDataSourceProtocol {
    
    var onTapHero: Bool = false
    var heroModel: HeroModel?
    
    func onTapHero(hero: HeroModel) {
        onTapHero = true
        heroModel = hero
    }
}

//
//  HeroRolesDataSourceMock.swift
//  HeroStatsTests
//
//  Created by Muchamad Rifki on 21/05/20.
//  Copyright © 2020 Muchamad Rifki. All rights reserved.
//

@testable import HeroStats

class HeroRolesDataSourceProtocolMock: HeroRolesDataSourceProtocol {
    var onTapRole: Bool = false
    var rolesSelected: String = ""
    func onTapRole(rolesSelected: String) {
        onTapRole = true
        self.rolesSelected = rolesSelected
    }
}
